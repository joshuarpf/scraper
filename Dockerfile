FROM python:3
RUN apt-get update
RUN apt-get install --assume-yes ffmpeg
RUN apt-get install --assume-yes graphviz
WORKDIR /usr/src/app
RUN pip install --upgrade pip
RUN pip install scrapy
RUN pip install scrapy-splash
RUN pip install -U linode-cli
RUN pip install boto 
RUN pip install --upgrade linode-cli
RUN pip install ffmpeg-python
RUN pip install graphviz
RUN pip install pymongo
RUN pip install -U lxml
RUN mkdir /root/.config/
COPY ./linode-cli /root/.config
COPY . /usr/src/app/
ENV XSCRAPER_ENV staging
ENV XSCRAPER_DATABASE mongodb://scraper:scr4p3rguy@mongo:27017/xscraper
ENV XSCRAPER_SPLASH http://splash:8050