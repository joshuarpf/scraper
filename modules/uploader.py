""" File that represents the file uploader module """
import abc
import os


class Uploader():
    """ Properties """

    def __init__(self, filename, storage_directory):
        self._filename = filename
        self._storage_directory = storage_directory

    @abc.abstractmethod
    def execute_upload(self):
        """ Interface for executing the upload command """

    @abc.abstractmethod
    def get_command(self) -> str:
        """ Interface to for generating a command """

    @abc.abstractmethod
    def get_public_url(self):
        """ Interface for generating the public url """

    @abc.abstractmethod
    def __generate_command_output_file(self):
        """ Interface for generating temp file """


class LinodeUploader(Uploader):
    """ Properties """

    _bucket_name = "xscraper"

    def __generate_command_output_file(self):
        """ Generates at temp file to hold the public_url value """
        # TODO: check the filename generation to clean this further
        filename = self._filename.replace('full/', '')
        command = "linode-cli obj signurl " + \
            self._bucket_name + " " + filename + " +3153600000 > " + \
            self._storage_directory + "/temp.txt"
        os.system(command)

    def get_command(self) -> str:
        """ Returns a string representing the command """
        command = "linode-cli obj put --acl-public " + self._storage_directory + "/" + \
            self._filename + " " + self._bucket_name
        return command

    def get_public_url(self):
        """ Returns a string that represents the public url """
        self.__generate_command_output_file()
        temp_file = open(self._storage_directory + '/temp.txt', 'r')

        if temp_file is not None:
            public_url = temp_file.read()
            return public_url

        return ""

    def execute_upload(self):
        """ Executes the upload """
        os.system(self.get_command())
