""" Imported depdendencies """
import random


class FilenameHelper():
    """ A helper class that simply generates a random hash filename """

    @staticmethod
    def get_filename(self) -> str:
        """ Generates a random hash filename """
        hash = random.getrandbits(128)
        return hash

    @staticmethod
    def get_filename_of_encoded_file(filename, suffix):
        """ Returns the output filename from a path """
        clean_filename = filename.replace("full/", "")
        return clean_filename + suffix
