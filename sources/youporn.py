""" Imported sources """
import logging
import logging.handlers
from lxml import etree
import re
from sources.porn_site import PornSite

LOGGER = logging.getLogger("")


class YouPorn(PornSite):
    """ Defines the Tube 8 Website surce """

    def fetch_video_download_url(self, html):
        DOM = etree.HTML(html)
        result = DOM.xpath("//meta[@name='twitter:player:stream']/@content")
        if result is not None:
            return result[0]
        return ""

    def get_cleaned_string(self, input_string):
        """ Strips down the uneccessary segments from the string """

        cleaned_string = input_string.replace('page_params.videoUrlJS = ', '')
        cleaned_string = cleaned_string.replace('\'', '')
        cleaned_string = cleaned_string.replace('"', '')
        cleaned_string = cleaned_string.replace(';', '')
        return cleaned_string

    def query_video_titles(self):
        """ Returns the XPATH string to query the top videos title """
        return "//div/div[3]/div/div[1]/div[2]/div[2]/div/a/div/text()"

    def query_video_urls(self) -> str:
        """ Returns an XPATH query to fetch the video URLS to be scraped """
        return "//div/div[3]/div/div[1]/div[2]/div[2]/div/a/@href"

    def query_video_download_url(self) -> str:
        """ Returns an XPATH query to get the download URL """
        return "/// "

    def query_video_title(self) -> str:
        """ Returns the query to fetch the title of the video to downloaded """
        return "//div/div[3]/div/div[1]/h1/text()"
