""" Site: underlying object representing a website """


class Site():
    """ Properties """

    """ Defines the constructor of a site """

    def __init__(self, name, url):
        self._name = name
        self._url = url

    def get_url(self) -> str:
        """ Returns the website URL """
        return self._url
