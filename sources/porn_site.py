""" Imported sources """
import abc
from sources.site import Site


class PornSite(abc.ABC, Site):
    """ Class definition of the parent Porn Site object """

    @abc.abstractmethod
    def fetch_video_download_url(self, html) -> str:
        """ Interfacing the query to fetch video download url """

    @abc.abstractmethod
    def query_video_titles(self):
        """ Interfacing get_query_for_video_titles """

    @abc.abstractmethod
    def query_video_title(self) -> str:
        """ Interface to return the query for video titles """

    @abc.abstractmethod
    def query_video_download_url(self) -> str:
        """ Interface to return the query for a video's doenload url """

    @abc.abstractmethod
    def query_video_urls(self) -> str:
        """ Interface to return the query for video urls """
