""" Imported sources """
import logging
import logging.handlers
import re
from sources.porn_site import PornSite

LOGGER = logging.getLogger("")


class VPorn(PornSite):
    """ Defines the Tube 8 Website surce """

    def __init__(self, name, url):
        super(VPorn, self).__init__(name, url)

    def fetch_video_download_url(self, html):
        """ Fetches video URL from a page """
        regex = r"src=\"(.)+.mp4\"\ type=\"video/mp4\" label=\"1080p\" prevent-default=\"true\" res=\"1080\" \/>"

        match = re.search(regex, html)

        download_url = ""

        if match is not None:
            tag = match.group(0)
            matching_link = re.search(r"https(.)*\.mp4", tag)

            if matching_link is not None:
                download_url = matching_link.group(0)
            else:
                LOGGER.debug("!!!!!!!!!!! NO MATCH FOR URL !!!!!!!!!!")
        else:
            LOGGER.debug(" !!!!!!!!!!! NO MATCH FOR TAG !!!!!!!!!!!!!!!!")

        return download_url

    def get_cleaned_string(self, input_string):
        """ Strips down the uneccessary segments from the string """

        cleaned_string = input_string.replace('page_params.videoUrlJS = ', '')
        cleaned_string = cleaned_string.replace('\'', '')
        cleaned_string = cleaned_string.replace('"', '')
        cleaned_string = cleaned_string.replace(';', '')
        return cleaned_string

    def query_video_titles(self):
        """ Returns the XPATH string to query the top videos title """
        return "/html/body/div[4]/div[3]/div/div/a/div/span/text()"

    def query_video_urls(self) -> str:
        """ Returns an XPATH query to fetch the video URLS to be scraped """
        return "/html/body/div[4]/div[3]/div/div/a/@href"

    def query_video_download_url(self) -> str:
        """ Returns an XPATH query to get the download URL """
        return "/// "

    def query_video_title(self) -> str:
        """ Returns the query to fetch the title of the video to downloaded """
        return "/html/body/div[4]/div[1]/div[1]/div[1]/div[1]/h1/text()"
