""" Imported sources """
import logging
import logging.handlers
import re
from sources.porn_site import PornSite

LOGGER = logging.getLogger("")


class Tube8(PornSite):
    """ Defines the Tube 8 Website surce """

    def fetch_video_download_url(self, html):

        regex = r"page_params\.videoUrlJS\ =(.)+;"

        match = re.search(regex, html)

        download_url = ""

        if (match is not None) & (match.group(0) is not None):
            output = self.get_cleaned_string(match.group(0))
            download_url = output
        else:
            LOGGER.debug("!!!!!!!!!! Tube8: Didn't match video url !!!!!!!!!")

        return download_url

    def get_cleaned_string(self, input_string):
        """ Strips down the uneccessary segments from the string """

        cleaned_string = input_string.replace('page_params.videoUrlJS = ', '')
        cleaned_string = cleaned_string.replace('\'', '')
        cleaned_string = cleaned_string.replace('"', '')
        cleaned_string = cleaned_string.replace(';', '')
        return cleaned_string

    def query_video_titles(self):
        """ Returns the XPATH string to query the top videos title """
        return "//div[@class='hot-porn-videos-wrapper']/div[@class='gridList']/figure/figcaption/p/a/text()"

    def query_video_urls(self) -> str:
        """ Returns an XPATH query to fetch the video URLS to be scraped """
        return "//div[@class='hot-porn-videos-wrapper']/div[@class='gridList']/figure/figcaption/p/a/@href"

    def query_video_download_url(self) -> str:
        """ Returns an XPATH query to get the download URL """
        return "/// "

    def query_video_title(self) -> str:
        """ Returns the query to fetch the title of the video to downloaded """
        return "//*[@id='videoWatchPage']/main/header/div/h1/text()"
