""" Imported dependencies """
import ffmpeg
import logging
import logging.handlers
from modules.uploader import LinodeUploader
from modules.uploader import Uploader
from pprint import pprint
import pymongo
import scrapy

from helpers.filename_helper import FilenameHelper
from scrapy.utils.project import get_project_settings
from scrapy.exceptions import DropItem
from scrapy.pipelines.files import FilesPipeline

LOGGER = logging.getLogger("")

SETTINGS = get_project_settings()

# TODO: figure out a way to read this from the config
FILE_STORAGE = "./tmp"
FILE_SUFFIX = "_xscraper.mp4"


class MongoDBPipeline:
    """ Class to save scraped data into mongodb """
    collection_name = 'videos'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        """ Returns the crawler settings """
        # pull in information from settings.py
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE'),
        )

    def open_spider(self, spider):
        """ Initializes the database connection """
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.database = self.client[self.mongo_db]

    def close_spider(self, spider):
        """ Closes the database connection """
        # clean up when spider is closed
        self.client.close()

    def process_item(self, item, spider):
        """ Executes the process of saving data into the database """
        insert_id = self.database[self.collection_name].insert_one(dict(item))
        if insert_id is not None:
            LOGGER.debug(">>>>>>>> INSERT ID: ")
            pprint(insert_id)
        return item


class FilesPipeline(FilesPipeline):
    """ Pipline to download the media from a site """

    def get_media_requests(self, item, info):
        LOGGER.debug(">>>>>>>> DOWNLOADING ... ")
        LOGGER.debug(item['download_url'])
        yield scrapy.Request(item['download_url'])

    def item_completed(self, results, item, info):
        file_paths = [x['path'] for ok, x in results if ok]
        if not file_paths:
            raise DropItem("Item contains no files")
        item['local_file_storage'] = file_paths[0]
        return item


class ResizePineline():
    """ Pipeline to lessen the video size """

    def process_item(self, item, spider):
        LOGGER.debug(" >>>>>> RESIZING VIDEO .... ")
        LOGGER.debug(item)
        output_filename = FilenameHelper.get_filename_of_encoded_file(
            item['local_file_storage'], FILE_SUFFIX)

        if output_filename is not None:
            process = (
                ffmpeg
                .input(FILE_STORAGE + "/" + item['local_file_storage'])
                .output(FILE_STORAGE + "/" + output_filename, crf=10)
                .global_args('-nostdin')
                .run()
            )

        else:
            LOGGER.error("!!! ResizePineline -> Failed to generate filename.")

        return item


class UploadPipeline:

    def process_item(self, item, spider):
        filename = FilenameHelper.get_filename_of_encoded_file(
            item['local_file_storage'], FILE_SUFFIX)

        if filename is not None:
            uploader = LinodeUploader(filename, FILE_STORAGE)
            uploader.execute_upload()
            public_url = uploader.get_public_url()
            item['public_url'] = public_url
        else:
            LOGGER.error("!!! UploadPipeline -> failed to generate filename.")

        return item
