# -*- coding: utf-8 -*-
""" Importing dependencies """
import logging
import logging.handlers
import random
import time

from xscraper.spiders.arachnid import Arachnid
from sources.youporn import YouPorn

LOGGER = logging.getLogger("")


class YouPornSpider(Arachnid):
    """ class definition for tube8 spider """
    name = 'youporn'
    website = YouPorn(name, "https://www.youporn.com")

    def __init__(self):
        super(YouPornSpider, self).__init__(self.website)

    def parse(self, response):
        """ Parses the site for contents """
        links = response.xpath(self.website.query_video_urls()).getall()

        #records_to_fetch = (random.randint(1, len(links)) - 1)
        # TODO: modify this to be configuratble
        records_to_fetch = 1

        for iterator in range(records_to_fetch):
            LOGGER.debug("SCRAPING #%s", iterator)
            max_index = len(links) - 1
            random_index = random.randint(0, max_index)
            link_to_fetch = "https://youporn.com" + links[random_index]
            yield response.follow(link_to_fetch, callback=self._parse_video_url)
            time.sleep(random.randint(0, 10))
