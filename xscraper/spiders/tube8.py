# -*- coding: utf-8 -*-
""" Importing dependencies """
import logging
import logging.handlers

from xscraper.spiders.arachnid import Arachnid
from sources.tube8 import Tube8

LOGGER = logging.getLogger("")


class Tube8Spider(Arachnid):
    """ class definition for tube8 spider """
    name = 'tube8'
    website = Tube8(name, "https://www.tube8.com")

    def __init__(self):
        super(Tube8Spider, self).__init__(self.website)
