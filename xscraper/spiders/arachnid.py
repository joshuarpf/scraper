""" Importing dependencies """
import logging
import logging.handlers
import random
import time
import scrapy
from scrapy_splash import SplashRequest

from xscraper.items import XscraperItem

LOGGER = logging.getLogger("")


class Arachnid(scrapy.Spider):
    """ This class inherits scrapy and is to be inherited by all spiders """
    __headers = {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.9",
        "Connection": "keep-alive",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36"
    }

    __scraped_data = XscraperItem()

    def __init__(self, website):
        self.website = website
        self.name = website._name

    def start_requests(self):
        url = self.website.get_url()
        yield SplashRequest(url, callback=self.parse, headers=self.__headers)

    def parse(self, response):
        """ Parses the site for contents """
        links = response.xpath(self.website.query_video_urls()).getall()

        #records_to_fetch = (random.randint(1, len(links)) - 1)
        # TODO: modify this to be configuratble
        records_to_fetch = 1

        for iterator in range(records_to_fetch):
            LOGGER.debug("SCRAPING #%s", iterator)
            max_index = len(links) - 1
            random_index = random.randint(0, max_index)
            link_to_fetch = links[random_index]
            yield response.follow(link_to_fetch, callback=self._parse_video_url)
            time.sleep(random.randint(0, 10))

    def __get_video_download_url(self, response):
        download_url = self.website.fetch_video_download_url(response.text)

        if download_url is not None:
            return download_url

        return ''

    def _parse_video_url(self, response):
        LOGGER.debug("SCRAPING: %s", response.url)
        result = response.xpath(self.website.query_video_title()).getall()
        if len(result) > 0:

            download_url = self.__get_video_download_url(response)
            self.__scraped_data['download_url'] = download_url

            video_title = result[0]
            original_url = response.url
            self.__scraped_data['original_url'] = original_url

            self.__scraped_data['title'] = video_title.strip()
            self.__scraped_data['source_name'] = self.name

            yield self.__scraped_data
