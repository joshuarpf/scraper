# -*- coding: utf-8 -*-
""" Importing dependencies """
import logging
import logging.handlers

from xscraper.spiders.arachnid import Arachnid
from sources.vporn import VPorn

LOGGER = logging.getLogger("")


class Tube8Spider(Arachnid):
    """ class definition for tube8 spider """
    name = 'vporn'
    website = VPorn(name, "https://www.vporn.com")

    def __init__(self):
        super(Tube8Spider, self).__init__(self.website)
