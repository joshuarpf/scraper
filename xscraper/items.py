""" Definition of sraped items """
# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class XscraperItem(scrapy.Item):
    """ Defines the fields for a scraped item """
    original_url = scrapy.Field()
    download_url = scrapy.Field()
    title = scrapy.Field()
    length = scrapy.Field()
    source_name = scrapy.Field()
    video_url = scrapy.Field()
    local_file_storage = scrapy.Field()
    public_url = scrapy.Field()
